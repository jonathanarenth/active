use active;
set FOREIGN_KEY_CHECKS = 0;
truncate HISTORY_TECHNICAL;
truncate TECHNICAL_INFORMATION;
truncate CONTRACT;
truncate TRANSPOSE;
truncate STATUS;
truncate RETURN_EXPERIENCE;
truncate REGISTER;
truncate AFFECTATION;
truncate INTERVENTION;
truncate CHARGE_STAGE;
truncate STAGE;
truncate EXPERIENCE;
truncate EMPLOYEE;
truncate JOBS;
truncate FIRM_DEPARTMENT;
truncate DOCUMENT;
truncate PROJECT;
truncate PROJECT_TYPE;
truncate CONTRIBUTE;
truncate CLIENT_CONTRIBUTOR;
truncate CLIENT;
set FOREIGN_KEY_CHECKS = 1;


/*==============================================================================================================*/
/* INSERT : CLIENT                                                                                              */
/*==============================================================================================================*/
insert into CLIENT (idClient, nameClient, typeClient, contactClient, fonctionContact, adressClient, phoneClient, mailClient, businessSector, nature, turnover, workforce, generalComment, specificComment) values (1, 'Céréales Lézard',   'privé',  'Ferand Hernandez',  'Acheteur',                 '45 route de loréal 45000 Rennes',   '02 54 85 23 56', 'f.hernandez@clezard.fr',             'agroalimentaire',                   'principale', 25400,  20,   NULL ,                         'paiement comptant');
insert into CLIENT (idClient, nameClient, typeClient, contactClient, fonctionContact, adressClient, phoneClient, mailClient, businessSector, nature, turnover, workforce, generalComment, specificComment) values (2, 'Pâtes Ménagères',   'privé',  'Michael Lorrain',   'Directeur Technique',      '67 rue des imposteurs 48650 Brest', '02 41 23 69 69', 'michael.lorrain@patesmenageres.com', 'agroalimentaire',                   'secondaire', 20360,  39,   NULL,                           NULL);
insert into CLIENT (idClient, nameClient, typeClient, contactClient, fonctionContact, adressClient, phoneClient, mailClient, businessSector, nature, turnover, workforce, generalComment, specificComment) values (3, 'Ville de Nantes',   'public', 'Olivier Tourquoin', 'Responsable informatique', 'Place de la ville 35254 Nantes',    '02 74 10 20 00', 'olivier.t@nantes.fr',                'communauté de communes',            'ancienne',   5000,   456, 'Maintenance des installation', 'facturatio à 90 jours');
insert into CLIENT (idClient, nameClient, typeClient, contactClient, fonctionContact, adressClient, phoneClient, mailClient, businessSector, nature, turnover, workforce, generalComment, specificComment) values (4, 'Croq Toutou',       'privé',  'Lisa Fontella',     'Acheteuse',                '67 impasse Perdue 48650 Brest',     '02 98 54 56 23', 'croqtoutou@croqtoutou.fr',           'fabrication aliment pour annimaux', 'principale', 100250, 52,  'très bon client',               NULL);
insert into CLIENT (idClient, nameClient, typeClient, contactClient, fonctionContact, adressClient, phoneClient, mailClient, businessSector, nature, turnover, workforce, generalComment, specificComment) values (5, 'Bretagne Céréales', 'privé',  'Laura Schmitt',     'Directeur Technique',      '2 Avenue des Voleurs 45000 Rennes', '02 54 54 54 65', 'laura.schmitt@bc.fr',                'Coopérative agricole',              'principale', 250000, 542, 'très bon client',              'facturation à 90 jours');

/*==============================================================================================================*/
/* INSERT : CLIENT_CONTRIBUTOR                                                                                  */
/*==============================================================================================================*/
insert into CLIENT_CONTRIBUTOR (idContributor, insideContributor, externContributor) values (1, 'Jaques Boudin',    NULL);
insert into CLIENT_CONTRIBUTOR (idContributor, insideContributor, externContributor) values (2, NULL,               'Rémy Gaillard');
insert into CLIENT_CONTRIBUTOR (idContributor, insideContributor, externContributor) values (3, 'Laure Bouquet',    NULL);
insert into CLIENT_CONTRIBUTOR (idContributor, insideContributor, externContributor) values (4, 'Henri Calvin',     NULL);
insert into CLIENT_CONTRIBUTOR (idContributor, insideContributor, externContributor) values (5, NULL,               'Florend Mariaud');
insert into CLIENT_CONTRIBUTOR (idContributor, insideContributor, externContributor) values (6, 'Stephane Clausen', NULL);

/*==============================================================================================================*/
/* INSERT : CONTRIBUTE                                                                                          */
/*==============================================================================================================*/
insert into CONTRIBUTE (idContributor, idClient) values (1, 3);
insert into CONTRIBUTE (idContributor, idClient) values (2, 1);
insert into CONTRIBUTE (idContributor, idClient) values (3, 2);
insert into CONTRIBUTE (idContributor, idClient) values (4, 2);
insert into CONTRIBUTE (idContributor, idClient) values (5, 4);
insert into CONTRIBUTE (idContributor, idClient) values (6, 5);

/*==============================================================================================================*/
/* INSERT : PROJECT_TYPE                                                                                        */
/*==============================================================================================================*/
insert into PROJECT_TYPE (idProjectType, labeltypeProject) values (1, 'forfait');
insert into PROJECT_TYPE (idProjectType, labeltypeProject) values (2, 'assistance');
insert into PROJECT_TYPE (idProjectType, labeltypeProject) values (3, 'régie');

/*==============================================================================================================*/
/* INSERT : PROJECT                                                                                             */
/*==============================================================================================================*/
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (2003, 2, 1, 'CRAME',  'CRM Pates Ménagères',              'Toutes',                                     '251',  3, 'RH',         9,  NULL,                                      'Traux sur SGBD',                     STR_TO_DATE ('12/01/2020', '%d/%m/%Y'), STR_TO_DATE ('31/03/2020', '%d/%m/%Y'), STR_TO_DATE ('14/01/2020', '%d/%m/%Y'), STR_TO_DATE ('27/03/2020', '%d/%m/%Y'));
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (2000, 3, 2, 'MAVIN',  'Maint Ville Nante',                'Programmation, Tests, Mise en production',   '120',  1, 'RH',         5,  'charge horaire réparie sur toutes année', 'Maintenance base RH',                STR_TO_DATE ('02/01/2020', '%d/%m/%Y'), STR_TO_DATE ('27/12/2020', '%d/%m/%Y'), STR_TO_DATE ('02/01/2020', '%d/%m/%Y'), NULL); 	
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (2005, 4, 3, 'CROQO',  'CRM Croq Toutou',                  'Toutes',                                     '335',  2, 'Production', 15, NULL,                                      NULL,                                 STR_TO_DATE ('03/03/2020', '%d/%m/%Y'), STR_TO_DATE ('05/06/2020', '%d/%m/%Y'), NULL,                                   NULL);		
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (1902, 5, 3, 'ERETAC', 'ERP Bretagne Céréales',            'Toutes',                                     '1100', 6, 'Tous',       35, NULL,                                      'ERP de gestion de toute entreprise', STR_TO_DATE ('01/07/2019', '%d/%m/%Y'), STR_TO_DATE ('30/12/2019', '%d/%m/%Y'), STR_TO_DATE ('05/07/2019', '%d/%m/%Y'), STR_TO_DATE ('30/12/2019', '%d/%m/%Y'));
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (2004, 1, 3, 'MOTEL',  'Modif Tables Céréales Lézard	',    'Base de données, Tests, Mise en production', '845',  2, 'Achat',      25, NULL,                                      'SGBD',                               STR_TO_DATE ('15/02/2020', '%d/%m/%Y'), STR_TO_DATE ('20/05/2020', '%d/%m/%Y'), STR_TO_DATE ('10/02/2020', '%d/%m/%Y'), NULL);	
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (2002, 5, 1, 'GEOBRE', 'Géolocalisation Bretane Céréales', 'Toutes',                                     '980',  6, 'Commercial', 31, NULL,                                      'Géolocalisation des voiures',        STR_TO_DATE ('02/01/2020', '%d/%m/%Y'), STR_TO_DATE ('07/02/2020', '%d/%m/%Y'), STR_TO_DATE ('02/01/2020', '%d/%m/%Y'), STR_TO_DATE ('10/02/2020', '%d/%m/%Y'));
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (1901, 5, 1, 'ATABRE', 'Ajout Table Bretagne Céréales',    'Base de données, Tests, Mise en production', '1400', 3, 'Stocks',     42, NULL,                                      'SGBD',                               STR_TO_DATE ('01/06/2019', '%d/%m/%Y'), STR_TO_DATE ('08/10/2019', '%d/%m/%Y'), STR_TO_DATE ('01/06/2019', '%d/%m/%Y'), STR_TO_DATE ('05/06/2019', '%d/%m/%Y'));
insert into PROJECT (idProject, idClient ,idProjectType, shortNameProject, longNameProject, lifeCycleProject, globalChargeEstimate, maximumSizeTeam, activitySector, siezeProject, saleComment, technicalComment, previsionalStartDate, previsionalEndDate, realStartDate, realEndDate) values (1900, 4, 1, 'GEOCRO', 'Géolocalisation Croq Toutou',      'Toutes',                                     '870',  6, 'Commercial', 28, NULL,                                      'Géolocalisation des livreurs',       STR_TO_DATE ('05/02/2019', '%d/%m/%Y'), STR_TO_DATE ('05/03/2019', '%d/%m/%Y'), STR_TO_DATE ('05/02/2019', '%d/%m/%Y'), STR_TO_DATE ('05/03/2019', '%d/%m/%Y'));

/*==============================================================================================================*/
/* INSERT : DOCUMENT                                                                                            */
/*==============================================================================================================*/
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (1,  2004, 'Geoloc code',      'Le code de Geolocalisation, pour adaptation a de futur projet');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (2,  1902, 'script base',      'script de la base ERP développée');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (3,  1901, 'MPD',              'Schéma MPD de la base de donnée');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (4,  2004, 'script code',      'Code Java des classes métiers');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (5,  2000, 'MPD',              'Schéma MPD du client');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (6,  2002, 'Algo RH',          'algroithme en pseudo code du code ');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (7,  2003, 'Test code',        'script des codes test');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (8,  1900, 'Geoloc algo',      'algorithme de la géolocalisation');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (9,  2005, 'Diagramme classe', 'Diagrame des classes métiers');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (10, 1901, 'Revue client',     'totalité des éléments validés par le client');
insert into DOCUMENT (idDocument, idProject, titleDocument, resumDocument) values (11, 1901, 'script SQL',       'script SQL du client');

/*==============================================================================================================*/
/* INSERT : FIRM_DEPARTMENT                                                                                     */
/*==============================================================================================================*/
insert into FIRM_DEPARTMENT (idArea, labelArea) values (1,  'Direction Générale');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (2,  'Direction Financière');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (3,  'Direction Administrative');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (4,  'Direction Commercial');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (5,  'Service Commercial');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (6,  'Secretariat Administratif');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (7,  'Département Etudes');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (8,  'Departement Projet');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (9,  'Ingénieur Etudes');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (10, 'Secretariat Technique');
insert into FIRM_DEPARTMENT (idArea, labelArea) values (11, 'Support Technique');

/*==============================================================================================================*/
/* INSERT : JOBS                                                                                                */
/*==============================================================================================================*/
insert into JOBS (idJobs, idArea, labelJobs) values (1, 1,   'Directeur Général');
insert into JOBS (idJobs, idArea, labelJobs) values (2, 2,   'Directeur Administratif et Financier');
insert into JOBS (idJobs, idArea, labelJobs) values (3, 3,   'Responsable des Ressources Humaines');
insert into JOBS (idJobs, idArea, labelJobs) values (4, 4,   'Responsable Commercial');
insert into JOBS (idJobs, idArea, labelJobs) values (5, 5,   'Commerial');
insert into JOBS (idJobs, idArea, labelJobs) values (6, 6,   'Secrétaire Administratif');
insert into JOBS (idJobs, idArea, labelJobs) values (7, 7,   'Responsable du Département Etudes');
insert into JOBS (idJobs, idArea, labelJobs) values (8, 8,   'Chef de Projet');
insert into JOBS (idJobs, idArea, labelJobs) values (9, 9,   'Analystes');
insert into JOBS (idJobs, idArea, labelJobs) values (10, 9,  'Développeur');
insert into JOBS (idJobs, idArea, labelJobs) values (11, 10, 'Secrétaire Technique');
insert into JOBS (idJobs, idArea, labelJobs) values (12, 11, 'Technicien Support');

/*==============================================================================================================*/
/* INSERT : EMPLOYEE                                                                                            */
/*==============================================================================================================*/
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (1, 8,   'MLLE', 'Véréna Nana',    'F', '1 rue de Flémard',       NULL,                           'Brest',            '48650', '02 54 56 58 52', '12345678');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (2, 9,   'MR',   'Olivier Pelé',   'M', '3 Impasse Perdue',	     'Résidence Paumée, 5ème étage', 'Rennes',           '45000', '02 14 20 32 56', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (3, 9,   'MR',   'Henry laconte',  'M', '2 Avnue Court Toujours', NULL,                           'Nantes',           '35250', '02 54 54 56 56', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (4, 9,   'MME',  'Isabelle Foula', 'F', '25 Rue de la Fête',      NULL,                           'Mont Saint Odile', '45830', '02 78 54 56 30', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (5, 8,   'MR',   'Ernest Pull',    'M', '45 Rue Oublie',          NULL,                           'Pontivy',          '45100', '02 85 23 65 45', '12335544');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (6, 8,   'MR',   'Mel Gibson',     'M', '75 Impasse du Libertin', NULL,                           'Bréan',            '42320', '02 54 66 33 99', '12121212');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (7, 10,  'MLLE', 'Sophie Davant',  'F', '38 rue enfance',         NULL,                           'Brest',            '48650', '02 11 23 55 55', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (8, 10,  'MR',   'Henry Calvin',   'M', '39 rue des Mouettes',    NULL,                           'Brest',            '48650', '02 87 52 12 22', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (9, 9,   'MR',   'Steeve Jobs',    'M', '85 Rue des Joncquières', NULL,                           'Rennes',           '45000', '02 55 44 99 88', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (10, 9,  'MLLE', 'Laura Hitachi',  'F', '12 rue Des Infirmes',    NULL,                           'Nantes',           '35250', '02 52 14 55 33', NULL);
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (11, 4,  'MR',   'Louis Colombus', 'F', '32 Impasse du Chômeur',  NULL,                           'Vannes',           '35500', '02 54 54 66 89', '11188866');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (12, 10, 'MR',   'Milo Zacaria',   'M', '16 Avenue du Flambeur',  NULL,                           'Vannes',           '35500', '02 54 56 88 77', '66644455');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (13, 11, 'MR',   'Julien Wicker',  'M', '18 Rue des imposteurs',  NULL,                           'Lohéac',           '45250', '02 54 66 87 88', '88844433');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (14, 3,  'MR',   'Mario Warrio',   'M', '4 Rue Infection',        NULL,                           'Saint Malo',       '45680', '02 66 69 99 55', '54543321');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (15, 7,  'MLLE', 'Lea Peach',      'F', '6 Rue des Practiciens',  NULL,                           'Saint Malo',       '45680', '02 54 65 65 88', '25558877');
insert into EMPLOYEE (idEmployee, idJobs, titleEmployee, nameEmployee, sexeEmployee, adress1Employee, adress2Employee, cityEmployee, zipCodeEmployee, phoneEmployee, login) values (16, 6,  'MR',   'Luke Skywalker', 'F', '7 Rue Louis Vuitton',    'Immeuble Lagardère',           'Nantes',           '35250', '02 45 78 96 33', '88774422');

/*==============================================================================================================*/
/* INSERT : EXPERIENCE                                                                                          */
/*==============================================================================================================*/
insert into EXPERIENCE (idExp, labelExp) values (1, 'Language Type Script');
insert into EXPERIENCE (idExp, labelExp) values (2, 'No SQL');
insert into EXPERIENCE (idExp, labelExp) values (3, 'Norme125466');
insert into EXPERIENCE (idExp, labelExp) values (4, 'Langauge Java');

/*==============================================================================================================*/
/* INSERT : STAGE                                                                                               */
/*==============================================================================================================*/
insert into STAGE (idLot, idProject, labelStage) values (200000, 2000, 'Analyse de la demande');
insert into STAGE (idLot, idProject, labelStage) values (200400, 2004, 'Mise au point de classe chantier');
insert into STAGE (idLot, idProject, labelStage) values (200200, 2002, 'Analyse de la demande');
insert into STAGE (idLot, idProject, labelStage) values (200101, 2004, 'Codage et commentaire');
insert into STAGE (idLot, idProject, labelStage) values (200001, 2000, 'Test et mise au point');

/*==============================================================================================================*/
/* INSERT : CHARGE_STAGE                                                                                        */
/*==============================================================================================================*/
insert into CHARGE_STAGE (idCharge, idLot, ProductionCharge, validationCharge, initialChargeEstimate) values (1, 200000, '12', '10', '20');
insert into CHARGE_STAGE (idCharge, idLot, ProductionCharge, validationCharge, initialChargeEstimate) values (2, 200400, '35', '20', '60');
insert into CHARGE_STAGE (idCharge, idLot, ProductionCharge, validationCharge, initialChargeEstimate) values (3, 200200, '10', '8', '15');
insert into CHARGE_STAGE (idCharge, idLot, ProductionCharge, validationCharge, initialChargeEstimate) values (4, 200101, '152', '165', '318');
insert into CHARGE_STAGE (idCharge, idLot, ProductionCharge, validationCharge, initialChargeEstimate) values (5, 200001, '120', '145', '240');

/*==============================================================================================================*/
/* INSERT : INTERVENTION                                                                                        */
/*==============================================================================================================*/
insert into INTERVENTION (idIntervention, idContributor, idLot, labelIntervention, startDate, endDate) values (1, 1, 200000, 'Base de donnée',              STR_TO_DATE ('05/02/2020', '%d/%m/%Y'), STR_TO_DATE ('07/02/2020', '%d/%m/%Y'));
insert into INTERVENTION (idIntervention, idContributor, idLot, labelIntervention, startDate, endDate) values (2, 2, 200200, NULL,                          STR_TO_DATE ('19/02/2020', '%d/%m/%Y'), STR_TO_DATE ('25/02/2020', '%d/%m/%Y'));
insert into INTERVENTION (idIntervention, idContributor, idLot, labelIntervention, startDate, endDate) values (4, 4, 200101, 'Programmation classe métier', STR_TO_DATE ('12/02/2020', '%d/%m/%Y'), NULL);
insert into INTERVENTION (idIntervention, idContributor, idLot, labelIntervention, startDate, endDate) values (5, 5, 200001, 'Analuse du besoin',           STR_TO_DATE ('18/01/2020', '%d/%m/%Y'), STR_TO_DATE ('20/01/2020', '%d/%m/%Y'));

/*==============================================================================================================*/
/* INSERT : AFFECTATION                                                                                         */
/*==============================================================================================================*/
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (1, 2, STR_TO_DATE ('05/02/2020', '%d/%m/%Y'), NULL);
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (2, 3, NULL,                                   STR_TO_DATE ('19/02/2020', '%d/%m/%Y'));
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (2, 6, STR_TO_DATE ('19/02/2020', '%d/%m/%Y'), NULL);
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (4, 7, STR_TO_DATE ('12/02/2020', '%d/%m/%Y'), NULL);
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (5, 8, STR_TO_DATE ('05/02/2020', '%d/%m/%Y'), NULL);
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (5, 9, STR_TO_DATE ('18/01/2020', '%d/%m/%Y'), NULL);
insert into AFFECTATION (idIntervention, idEmployee, dateAffectation, dateTest) values (1, 3, STR_TO_DATE ('17/03/2020', '%d/%m/%Y'), NULL);

/*==============================================================================================================*/
/* INSERT : REGISTER                                                                                            */
/*==============================================================================================================*/
insert into REGISTER (idClient, idEmployee, dateRegister) values (1, 10, NULL);
insert into REGISTER (idClient, idEmployee, dateRegister) values (2, 10, STR_TO_DATE('01/02/2018', '%d/%m/%Y'));
insert into REGISTER (idClient, idEmployee, dateRegister) values (3, 10, STR_TO_DATE('02/02/2019', '%d/%m/%Y'));
insert into REGISTER (idClient, idEmployee, dateRegister) values (4, 10, STR_TO_DATE('03/02/2018', '%d/%m/%Y'));
insert into REGISTER (idClient, idEmployee, dateRegister) values (5, 10, STR_TO_DATE('04/02/2018', '%d/%m/%Y'));

/*==============================================================================================================*/
/* INSERT : RETURN_EXPERIENCE                                                                                   */
/*==============================================================================================================*/
insert into RETURN_EXPERIENCE (idProject, idExp) values (1900, 1);
insert into RETURN_EXPERIENCE (idProject, idExp) values (1901, 2);
insert into RETURN_EXPERIENCE (idProject, idExp) values (2002, 3);
insert into RETURN_EXPERIENCE (idProject, idExp) values (2003, 4);

/*==============================================================================================================*/
/* INSERT : TRANSPOSE                                                                                           */
/*==============================================================================================================*/
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 1,  STR_TO_DATE ('12/01/2020', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 2,  STR_TO_DATE ('12/02/2020', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 3,  STR_TO_DATE ('12/04/2020', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 4,  STR_TO_DATE ('16/12/2019', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 5,  STR_TO_DATE ('08/12/2019', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 6,  STR_TO_DATE ('06/05/2020', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 7,  STR_TO_DATE ('07/08/2020', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 8,  STR_TO_DATE ('19/06/2019', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 9,  STR_TO_DATE ('19/08/2019', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 10, STR_TO_DATE ('19/06/2019', '%d/%m/%Y'));
insert into TRANSPOSE (idEmployee, idDocument, saveDate) values (13, 11, STR_TO_DATE ('02/02/2020', '%d/%m/%Y'));

/*==============================================================================================================*/
/* INSERT : STATUS                                                                                              */
/*==============================================================================================================*/
insert into STATUS (idStatus, labelStatus) values (1, 'CDI');
insert into STATUS (idStatus, labelStatus) values (2, 'CDD');
insert into STATUS (idStatus, labelStatus) values (3, 'STA');

/*==============================================================================================================*/
/* INSERT : CONTRACT                                                                                            */
/*==============================================================================================================*/ 
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 1,	7,	NULL, NULL, STR_TO_DATE ('12/05/2015', '%d/%m/%Y'),	NULL,                                   67000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 2,	8,	NULL, NULL, STR_TO_DATE ('12/01/1990', '%d/%m/%Y'),	NULL,                                   45000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 3,	8,	NULL, NULL, STR_TO_DATE ('12/01/1996', '%d/%m/%Y'),	NULL,                                   63000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 4,	8,	NULL, NULL, STR_TO_DATE ('12/01/2005', '%d/%m/%Y'),	NULL,                                   85000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (3, 5,	7,	NULL, NULL, STR_TO_DATE ('12/01/2019', '%d/%m/%Y'),	NULL,                                   NULL);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 6,	7,	NULL, NULL, STR_TO_DATE ('12/01/2018', '%d/%m/%Y'),	NULL,                                   48000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (2, 7,	8,	NULL, NULL, STR_TO_DATE ('02/02/2020', '%d/%m/%Y'),	STR_TO_DATE ('09/08/2020', '%d/%m/%Y'),	27500);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (2, 8,	8,	NULL, NULL, STR_TO_DATE ('23/04/2019', '%d/%m/%Y'),	STR_TO_DATE ('31/10/2020', '%d/%m/%Y'),	39000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 9,	8,	NULL, NULL, STR_TO_DATE ('06/04/2013', '%d/%m/%Y'),	NULL,                                   29000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 10,	8,	NULL, NULL, STR_TO_DATE ('06/02/2003', '%d/%m/%Y'),	NULL,                                   45000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 11,	3,	NULL, NULL, STR_TO_DATE ('06/09/2004', '%d/%m/%Y'),	NULL,                                   25000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 12,	9,	NULL, NULL, STR_TO_DATE ('06/01/1992', '%d/%m/%Y'),	NULL,                                   35000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 13,	10,	NULL, NULL, STR_TO_DATE ('06/04/2007', '%d/%m/%Y'),	NULL,                                   124000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 14,	2,	NULL, NULL, STR_TO_DATE ('25/07/2009', '%d/%m/%Y'),	NULL,                                   33000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 15,	6,	NULL, NULL, STR_TO_DATE ('26/07/2012', '%d/%m/%Y'),	NULL,                                   24000);
insert into CONTRACT (idStatus, idEmployee, idJobs, dateAugmentation, datePromotion, recruitmentDate, contractEndDate, salary) values (1, 16,	5,	NULL, NULL, STR_TO_DATE ('25/04/2018', '%d/%m/%Y'),	NULL,                                   72000);

/*==============================================================================================================*/
/* INSERT : TECHNICAL_INFORMATION                                                                               */
/*==============================================================================================================*/
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (1, 'Language Java');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (2, 'No SQL MongoDB');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (3, 'SQL Maria DB');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (4, 'Angular 8 + C#');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (5, 'C++');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (6, 'Type Script');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (7, 'Java');
insert into TECHNICAL_INFORMATION (idTechnicalInfo, labelTechnicalInfo) values (8, 'SQL');

/*==============================================================================================================*/
/* INSERT : TECHNICAL_INFORMATION                                                                               */
/*==============================================================================================================*/
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (1, 2003, STR_TO_DATE ('31/03/2020', '%d/%m/%Y'));
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (2, 2000, NULL);
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (3, 2005, NULL);
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (4, 1902, STR_TO_DATE ('05/01/2020', '%d/%m/%Y'));
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (5, 2004, NULL);
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (6, 2002, STR_TO_DATE ('15/02/2020', '%d/%m/%Y'));
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (7, 1901, STR_TO_DATE ('10/06/2019', '%d/%m/%Y'));
insert into HISTORY_TECHNICAL (idTechnicalInfo, idProject, dateInfo) values (8, 1900, STR_TO_DATE ('10/03/2019', '%d/%m/%Y'));

/*====================================================================================================*/
/* Test update Status                                                                                 */
/*====================================================================================================*/
-- update STATUS
-- set labelStatus = 'CDI'
-- where idStatus = 2;


