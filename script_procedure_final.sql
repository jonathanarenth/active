use active;

/*========================================================================================*/
/* Procedure 1: avarage charge project estimate                                           */
/*========================================================================================*/
drop procedure if exists average_project_charge;

delimiter $$
create procedure average_project_charge ()
begin
    select avg (globalChargeEstimate)
    from PROJECT;
end $$
delimiter ;
call average_project_charge();

/*========================================================================================*/
/* Procedure 2: list of project terminate since 2 years on thechnical theme               */
/*========================================================================================*/
drop procedure if exists list_project_with_thechnical_theme;

delimiter $$
create procedure list_project_with_thechnical_theme ()
begin 
    select shortNameProject, labelTechnicalInfo
    from PROJECT P
    inner join HISTORY_TECHNICAL HT on P.idProject = HT.idProject
    left join TECHNICAL_INFORMATION TI on HT.idTechnicalInfo = TI.idTechnicalInfo
    where labelTechnicalInfo like '%Java%' and datediff (current_date, realEndDate) >60;
end $$
delimiter ;
call list_project_with_thechnical_theme ();

/*========================================================================================*/
/* Procedure 3: Intervention of employee on project                                       */
/*========================================================================================*/
drop procedure if exists intervention_employee_on_project;

delimiter $$
create procedure intervention_employee_on_project ()
begin
    select E.nameEmployee, J.labelJobs, I.labelIntervention, I.startDate, I.endDate
    from EMPLOYEE E
    inner join JOBS J on E.idJobs = J.idJobs
    left join AFFECTATION A on E.idEmployee = A.idEmployee
    left join INTERVENTION I on A.idIntervention = I.idIntervention
    where startDate is not null;
end $$
delimiter ;
call intervention_employee_on_project