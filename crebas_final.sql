drop table if exists HISTORY_TECHNICAL;
drop table if exists TECHNICAL_INFORMATION;
drop table if exists CONTRACT;
drop table if exists TRANSPOSE;
drop table if exists STATUS;
drop table if exists RETURN_EXPERIENCE;
drop table if exists REGISTER;
drop table if exists AFFECTATION;
drop table if exists INTERVENTION;
drop table if exists CHARGE_STAGE;
drop table if exists STAGE;
drop table if exists EXPERIENCE;
drop table if exists EMPLOYEE;
drop table if exists JOBS;
drop table if exists FIRM_DEPARTMENT;
drop table if exists DOCUMENT;
drop table if exists PROJECT;
drop table if exists PROJECT_TYPE;
drop table if exists CONTRIBUTE;
drop table if exists CLIENT_CONTRIBUTOR;
drop table if exists CLIENT;

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT 
(
   idClient             integer                        not null auto_increment,
   nameClient           varchar(256)                   not null,
   typeClient           char(6)                        not null,
   contactClient        varchar(256)                   not null,
   fonctionContact      varchar(256)                   not null,
   adressClient         varchar(256)                   not null,
   phoneClient          varchar(20)                    not null,
   mailClient           varchar(256)                   not null,
   businessSector       varchar(256)                   not null,
   nature               char(10)                       not null,
   turnover             numeric(30)                    not null,
   workforce            integer                        not null,
   generalComment       varchar(500)                   null,
   specificComment      varchar(500)                   null,
   constraint PK_CLIENT primary key (idClient)
);

/*==============================================================*/
/* Table : CLIENT_CONTRIBUTOR                                   */
/*==============================================================*/
create table CLIENT_CONTRIBUTOR 
(
   idContributor        integer                        not null auto_increment,
   insideContributor    varchar(256)                   null,
   externContributor    varchar(256)                   null,
   constraint PK_CLIENT_CONTRIBUTOR primary key (idContributor)
);

/*==============================================================*/
/* Table : CONTRIBUTE                                           */
/*==============================================================*/
create table CONTRIBUTE 
(
   idContributor        integer                        not null,
   idClient             integer                        not null,
   constraint PK_CONTRIBUTE primary key (idContributor, idClient),
   constraint FK_CONTRIBUTE_CLIENT_CONTRIBUTOR foreign key (idContributor)
   references CLIENT_CONTRIBUTOR (idContributor),
   constraint FK_CONTRIBUTE_CLIENT foreign key (idClient)
   references CLIENT (idClient)
);

/*==============================================================*/
/* Table : PROJECT_TYPE                                         */
/*==============================================================*/
create table PROJECT_TYPE 
(
   idProjectType        integer                        not null auto_increment,
   labeltypeProject     varchar(256)                   not null,
   constraint PK_PROJECT_TYPE primary key (idProjectType)
);

/*==============================================================*/
/* Table : PROJECT                                              */
/*==============================================================*/
create table PROJECT 
(
   idProject            integer                        not null auto_increment,
   idClient             integer                        not null,
   idProjectType        integer                        not null,
   shortNameProject     char(10)                        not null,
   longNameProject      varchar(50)                    not null,
   lifeCycleProject     varchar(256)                   not null,
   globalChargeEstimate integer                           null,
   maximumSizeTeam      integer                        not null,
   activitySector       varchar(50)                    not null,
   siezeProject         integer                        not null,
   saleComment          varchar(256)                   null,
   technicalComment     varchar(256)                   null,
   previsionalStartDate date                           not null,
   previsionalEndDate   date                           not null,
   realStartDate        date                           null,
   realEndDate          date                           null,
   constraint PK_PROJECT primary key (idProject),
   constraint FK_PROJECT_CLIENT foreign key (idClient)
   references CLIENT (idClient),
   constraint FK_PROJECT_PROJECT_TYPE foreign key (idProjectType)
   references PROJECT_TYPE (idProjectType)
);

/*==============================================================*/
/* Table : DOCUMENT                                             */
/*==============================================================*/
create table DOCUMENT 
(
   idDocument           integer                        not null auto_increment,
   idProject            integer                        not null,
   titleDocument        varchar(50)                    not null,
   resumDocument        varchar(256)                   not null,
   constraint PK_DOCUMENT primary key (idDocument),
   constraint FK_DOCUMENT_PROJECT foreign key (idProject)
   references PROJECT (idProject)
   on delete cascade
);

/*==============================================================*/
/* Table : FIRM_DEPARTMENT                                       */
/*==============================================================*/
create table FIRM_DEPARTMENT 
(
   idArea               integer                        not null auto_increment,
   labelArea            varchar(50)                    not null,
   constraint PK_FIRM_DEPARTMENT primary key (idArea)
);

/*==============================================================*/
/* Table : JOBS                                             */
/*==============================================================*/
create table JOBS 
(
   idJobs               integer                        not null auto_increment,
   idArea               integer                        not null,
   labelJobs            varchar(50)                    not null,
   constraint PK_JOBS primary key (idJobs),
   constraint FK_JOBS_FIRM_DEPARTMENT foreign key (idArea)
   references FIRM_DEPARTMENT (idArea)
);

/*==============================================================*/
/* Table : EMPLOYEE                                             */
/*==============================================================*/
create table EMPLOYEE 
(
   idEmployee           integer                        not null auto_increment,
   idJobs               integer                        not null,
   titleEmployee        char(4)                        not null,
   nameEmployee         varchar(256)                   not null,
   sexeEmployee         char(1)                        not null,
   adress1Employee      varchar(256)                   not null,
   adress2Employee      varchar(256)                   null,
   cityEmployee         varchar(256)                   not null,
   zipCodeEmployee      char(5)                        not null,
   phoneEmployee        varchar(20)                    not null,
   login                char(8)                        null,
   constraint PK_EMPLOYEE primary key (idEmployee),
   constraint FK_EMPLOYEE_JOBS foreign key (idJobs)
   references JOBS (idJobs)
);

/*==============================================================*/
/* Table : EXPERIENCE                                           */
/*==============================================================*/
create table EXPERIENCE 
(
   idExp                integer                        not null auto_increment,
   labelExp             varchar(256)                   not null,
   constraint PK_EXPERIENCE primary key (idExp)
);

/*==============================================================*/
/* Table : STAGE                                                */
/*==============================================================*/
create table STAGE 
(
   idLot                integer                        not null auto_increment,
   idProject            integer                        not null,
   LabelStage           varchar(50)                    not null,
   constraint PK_STAGE primary key (idLot),
   constraint FK_STAGE_PROJECT foreign key (idProject)
   references PROJECT (idProject)
   on delete cascade
);

/*==============================================================*/
/* Table : CHARGE_STAGE                                         */
/*==============================================================*/
create table CHARGE_STAGE 
(
   idCharge             integer                        not null auto_increment,
   idLot                integer                        not null,
   ProductionCharge     integer                           not null,
   validationCharge     integer                           not null,
   initialChargeEstimate integer                           not null,
   constraint PK_CHARGE_STAGE primary key (idCharge),
   constraint FK_CHARGE_STAGE_STAGE foreign key (idLot)
   references STAGE (idLot)
);

/*==============================================================*/
/* Table : INTERVENTION                                         */
/*==============================================================*/
create table INTERVENTION 
(
   idIntervention       integer                        not null auto_increment,
   idContributor        integer                        null,
   idLot                integer                        null,
   labelIntervention    varchar(256)                   null,
   startDate            date                           null,
   endDate              date                           null,
   constraint PK_INTERVENTION primary key (idIntervention),
   constraint FK_INTERVENTION_CLIENT_CONTRIBUTOR foreign key (idContributor)
   references CLIENT_CONTRIBUTOR (idContributor),
   constraint FK_INTERVENTION_STAGE foreign key (idLot)
   references STAGE (idLot)
);

/*==============================================================*/
/* Table : AFFECTATION                                          */
/*==============================================================*/
create table AFFECTATION 
(
   idIntervention       integer                        not null,
   idEmployee           integer                        not null,
   dateAffectation      date                           null,
   dateTest             date                           null,
   constraint PK_AFFECTATION primary key (idIntervention, idEmployee),
   constraint FK_AFFECTATION_INTERVENTION foreign key (idIntervention)
   references INTERVENTION (idIntervention),
   constraint FK_AFFECTATION_EMPLOYEE foreign key (idEmployee)
   references EMPLOYEE (idEmployee) 
);

/*==============================================================*/
/* Table : REGISTER                                             */
/*==============================================================*/
create table REGISTER 
(
   idClient             integer                        not null,
   idEmployee           integer                        not null,
   dateRegister         date                           null,
   constraint PK_REGISTER primary key (idClient, idEmployee),
   constraint FK_REGISTER_CLIENT foreign key (idClient)
   references CLIENT (idClient),
   constraint FK_REGISTER_EMPLOYEE foreign key (idEmployee)
   references EMPLOYEE (idEmployee)
);

/*==============================================================*/
/* Table : RETURN_EXPERIENCE                                    */
/*==============================================================*/
create table RETURN_EXPERIENCE 
(
   idProject            integer                        not null,
   idExp                integer                        not null,
   constraint PK_RETURN_EXPERIENCE primary key (idProject, idExp),
   constraint FK_RETURN_EXPERIENCE_PROJECT foreign key (idProject)
   references PROJECT (idProject)
   on delete cascade,
   constraint FK_RETURN_EXPERIENCE_EXPERIENCE foreign key (idExp)
   references EXPERIENCE (idExp)
);

/*==============================================================*/
/* Table : TRANSPOSE                                            */
/*==============================================================*/
create table TRANSPOSE 
(
   idEmployee           integer                        not null,
   idDocument           integer                        not null,
   saveDate             date                           null,
   constraint PK_TRANSPOSE primary key (idEmployee, idDocument),
   constraint FK_TRANSPOSE_EMPLOYEE foreign key (idEmployee)
   references EMPLOYEE (idEmployee),
   constraint FK_TRANSPOSE_DOCUMENT foreign key (idDocument)
   references DOCUMENT (idDocument)
   on delete cascade
);

/*==============================================================*/
/* Table : STATUS                                               */
/*==============================================================*/
create table STATUS 
(
   idStatus             integer                        not null auto_increment,
   labelStatus          varchar(20)                    not null,
   constraint PK_STATUS primary key (idStatus)
);

/*==============================================================*/
/* Table : CONTRACT                                             */
/*==============================================================*/
create table CONTRACT 
(
   idStatus             integer                        not null,
   idEmployee           integer                        not null,
   idJobs               integer                        not null,
   dateAugmentation     date                           null,
   datePromotion        date                           null,
   recruitmentDate      date                           not null,
   contractEndDate      date                           null,
   salary               numeric(8)                     null,
   constraint PK_CONTRACT primary key (idStatus, idEmployee, idJobs),
   constraint FK_CONTRACT_STATUS foreign key (idStatus)
   references STATUS (idStatus),
   constraint FK_CONTRACT_EMPLOYEE foreign key (idEmployee)
   references EMPLOYEE (idEmployee),
   constraint FK_CONTRACT_JOBS foreign key (idJobs)
   references JOBS (idJobs)
);

/*==============================================================*/
/* Table : TECHNICAL_INFORMATION                                 */
/*==============================================================*/
create table TECHNICAL_INFORMATION 
(
   idTechnicalInfo      integer                        not null auto_increment,
   labelTechnicalInfo   varchar(256)                   not null,
   constraint PK_TECHNICAL_INFORMATION primary key (idTechnicalInfo)
);

/*==============================================================*/
/* Table : HISTORY_TECHNICAL                                    */
/*==============================================================*/
create table HISTORY_TECHNICAL 
(
   idTechnicalInfo      integer                        not null,
   idProject            integer                        not null,
   dateInfo             date                           null,
   constraint PK_HISTORY_TECHNICAL primary key (idTechnicalInfo, idProject),
   constraint FK_HISTORY_TECHNICAL_TECHNICAL_INFORMATION foreign key (idTechnicalInfo)
   references TECHNICAL_INFORMATION (idTechnicalInfo),
   constraint FK_HISTORY_TECHNICAL_PROJECT foreign key (idProject)
   references PROJECT (idProject)
   on delete cascade
);

/*============================================================================*/
/* Trigger project:  Start and End prevional date of project                  */
/*============================================================================*/
drop trigger if exists verifPrevisionnalDate

delimiter $$
create trigger verifPrevisionalDate
before insert
on PROJECT for each row
begin 
    if new.previsionalStartDate > new.previsionalEndDate
      then signal sqlstate '45000'
         set message_text = "The previsional Start date cannot be higher than the previsional End date";
    end if; 
end $$
delimiter ;

/*============================================================================*/
/* Trigger client:  verification of turnover while workforce                  */
/*============================================================================*/
drop trigger if exists client_turnover_is_valid

delimiter $$
create trigger client_turnover_is_valid
before insert
on CLIENT for each row
begin 
   if new.turnover / new.workforce > 1000000
      then signal sqlstate '45000'
         set message_text = "The ratio between turnover and workforce exceeds one million";
   end if;
end $$
delimiter ;

/*============================================================================*/
/* Trigger uptade:  Consistency of status                                     */
/*============================================================================*/
drop trigger if exists consistency_status

delimiter $$
create trigger consistency_status
after update
on STATUS for each row
begin
   if old.labelStatus = 'CDI'
      then
         case 
            when new.labelStatus = 'CDD' 
             then signal sqlstate '45000'
               set message_text= "CDI can't be converted in CDD";
            when new.labelStatus = 'STA' 
             then signal sqlstate '45000' 
               set message_text= "CDI can't be converted in STA";
         end case;
   end if;
   if old.labelStatus = 'CDD'
      then
         case
            when new.labelStatus = 'STA' 
             then signal sqlstate '45000'
               set message_text= "CDD can't be converted in STA";
         end case;
   end if;
end $$
delimiter ;

/*=================================================================================*/
/* Trigger delete: not delete project if real date end < 2 mouth current date      */
/*=================================================================================*/
 drop trigger if exists not_detete_project

 delimiter $$
 create trigger not_delete_project
 before delete
 on PROJECT for each row
 begin
    if datediff (current_date, old.realEndDate) < 60
       then signal sqlstate '45000'
          set message_text = "impossible to delete project if end date is < 2 mouth";
    end if;
 end $$
 delimiter ;
