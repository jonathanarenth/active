use active;

/*============================================================================*/
/* Complaint 1: Average charge estimate per activity sector                   */
/*============================================================================*/
select activitySector, avg(globalChargeEstimate)
from PROJECT
group by activitySector
order by globalChargeEstimate;

/*============================================================================*/
/* Complaint 2: Intervention and function of employee i project                  */
/*============================================================================*/
select shortNameproject, nameEmployee, labelJobs
from EMPLOYEE E
inner join JOBS J 
on E.idJobs = J.idJobs
inner join AFFECTATION A 
on E.idEmployee = A.idEmployee
inner join INTERVENTION I 
on A.idIntervention = I.idIntervention
inner join STAGE S 
on I.idLot = S.idLot
inner join PROJECT P 
on P. idProject = S.idProject;

/*============================================================================*/
/* Complaint 3: project list current date, short by activity sector, with number
of employee with treir function                                               */
/*============================================================================*/
select P.shortNameproject , P.activitySector, count(E.idEmployee), J.labelJobs
from PROJECT P
inner join STAGE S 
on P.idProject = S.idProject
inner join INTERVENTION I 
on S.idLot = I.idLot
inner join AFFECTATION A 
on I.idIntervention = A.idIntervention
inner join EMPLOYEE E 
on A.idEmployee = E.idEmployee
inner join JOBS J 
on E.idJobs = J.idJobs
where realEndDate is null and realStartDate is not null
group by labeljobs, activitySector;

/*============================================================================*/
/* Complaint uptade : increase of 5% if more 5 years senority                 */
/*============================================================================*/
select nameEmployee, salary*1.05, recruitmentDate
from contract C
inner join EMPLOYEE E 
on C.idEmployee = E.idEmployee
where datediff (current_date, recruitmentDate)> 1825;

/*============================================================================*/
/* Complaint uptade : delete end project with not stage                       */
/*============================================================================*/
set SQL_SAFE_UPDATES =0;
delete P
from PROJECT P
left join STAGE S 
on P.idProject = S.idProject
where realEndDate is not null and labelStage is null;
set SQL_SAFE_UPDATES =1;

/* vérification de la supression*/
-- select *
-- from PROJECT P
-- left join STAGE S on P.idProject = S.idProject;

/* verif delete projet >2 mois */
-- set SQL_SAFE_UPDATES =0;
-- delete P
-- from PROJECT P
-- where idProject =2005;
-- set SQL_SAFE_UPDATES =1;

/* verif impossible delete projet <2 mois */
-- set SQL_SAFE_UPDATES =0;
-- delete P
-- from PROJECT P
-- where datediff (current_date, realEndDate)<60;
-- set SQL_SAFE_UPDATES =1;